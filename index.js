const jsonServer = require("json-server");
const players = require("./models/players.json");
const teams = require("./models/teams.json");
const favorites = [];

const models = {
    players,
    teams,
    favorites
};

const db = Object.keys(models).reduce((prevVal, modelKey) => {
    prevVal[modelKey] = models[modelKey].map(item => {
        return { ...item.fields, id: item.id };
    });

    return prevVal;
}, {});

const relations = [
    {
        name: "teams",
        relation: [{ alias: "players", table: "players", name: "team" }]
    }
];

const addRelation = (db, host, relations) => {
    return relations.reduce((prevVal, relation) => {
        const alias = relation.alias;
        const table = relation.table;
        const name = relation.name;

        prevVal[alias] = db[table].filter(item => item[name] === host.id).map(item => item.id);
        return prevVal;
    }, {});
};

const relationDb = relations.reduce((prevVal, current) => {
    prevVal[current.name] = prevVal[current.name].map(item => {
        return { ...item, ...addRelation(prevVal, item, current.relation) };
    });
    return prevVal;
}, db);

const server = jsonServer.create();
server.use(jsonServer.defaults());

const router = jsonServer.router(relationDb);
server.use(router);

console.log("API on port 3008");

server.listen(3008);
